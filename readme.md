Think Walsh Datatypes
======================

This extension makes available extra datatypes in the eZ Publish Legacy Stack.

It was created to handle the issue of not being able to index / sort the float datatype.

At preset this datatype exist as "Extended Float"

Installation 
--------------

1) Checkout / Download the repo into <ez publish root>/extension/tw_datatypes

2) Activate extension

3) Regenerate autoload arrays

4) Clear the caches

Notes 
------

Extended Float is a direct copy of the existing ezFloat datatype with sorting and indexing added to the class.